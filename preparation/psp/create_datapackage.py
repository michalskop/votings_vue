"""Creates datapackage from downloaded files from http://www.psp.cz/sqw/hp.sqw?k=1300 ."""

from contextlib import closing
import codecs
import csv
import locale
import requests
import os

# path
try:
    path = os.path.dirname(os.path.realpath(__file__))
except Exception:
    path = os.getcwd()

path = '/home/michal/project/votings/dev/csv/psp/'
term = '172'
group_number = '1'
region_number = '75'
memberships = {
    'commissions': "2",
    "committees": "3",
    "delegations": "7"
}

anals = [
    {
        "name": "",
        "min": 0,
        "max": 1000000
    },
    # {
    #     "name": "_vlada1",
    #     "min": 67018,
    #     "max": 67896    # 15.6.2019
    # },
    # {
    #     "name": "_vlada2",
    #     "min": 67896,
    #     "max": 69449    # 16.3.2019
    # },
    # {
    #     "name": "_left",
    #     "min": 69449,
    #     "max": 70880
    # }
]

# prepare data
locale.setlocale(locale.LC_ALL, 'cs_CZ.UTF-8')
poslanec = {}
organy = {}
osoby = {}
zarazeni = []
ve = []
zmatecne = []
votes = []
data_obj = {}
data_all_obj = {}
data = []
data_all = []
with open(path + "poslanec.unl", encoding="cp1250") as fin:
    csvr = csv.reader(fin, delimiter='|')
    for row in csvr:
        poslanec[row[0]] = row

with open(path + "organy.unl", encoding="cp1250") as fin:
    csvr = csv.reader(fin, delimiter='|')
    for row in csvr:
        organy[row[0]] = row

with open(path + "osoby.unl", encoding="cp1250") as fin:
    csvr = csv.reader(fin, delimiter='|')
    for row in csvr:
        osoby[row[0]] = row

with open(path + "zarazeni.unl", encoding="cp1250") as fin:
    csvr = csv.reader(fin, delimiter='|')
    for row in csvr:
        zarazeni.append(row)

with open(path + "hl2017s.unl", encoding="cp1250") as fin:
    csvr = csv.reader(fin, delimiter='|')
    for row in csvr:
        ve.append(row)

with open(path + "zmatecne.unl", encoding="cp1250") as fin:
    csvr = csv.reader(fin, delimiter='|')
    for row in csvr:
        zmatecne.append(row[0])

with open(path + "hl2017h1.unl", encoding="cp1250") as fin:
    csvr = csv.reader(fin, delimiter='|')
    for row in csvr:
        votes.append(row)

with open(path + "hl2017h2.unl", encoding="cp1250") as fin:
    csvr = csv.reader(fin, delimiter='|')
    for row in csvr:
        votes.append(row)

pparties = {}
url = "https://raw.githubusercontent.com/michalskop/political_parties/master/cz/parties.csv"
with closing(requests.get(url, stream=True)) as r:
    csvr = csv.DictReader(codecs.iterdecode(r.iter_lines(), 'utf-8'))
    for row in csvr:
        pparties[row['abbreviation']] = row
# manual correction:
pparties['TOP09'] = pparties['TOP 09']

# snemovna
for row in zarazeni:
    if row[1] == term and row[4] == '':
        data_obj[row[0]] = {
            "id": row[0],
            "committees": [],
            "delegations": [],
            "commissions": []
        }
    if row[1] == term:
        data_all_obj[row[0]] = {
            "id": row[0],
            "committees": [],
            "delegations": [],
            "commissions": []
        }

# prepare directory
if not os.path.exists(path + "data"):
    os.makedirs(path + "data")

# kluby
groups = []
with open(path + "data/groups.csv", "w") as fout:
    header = ["id", "name", "abbreviation", "color"]
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    for k in organy:
        if organy[k][1] == term and organy[k][2] == group_number:
            groups.append(organy[k][0])
            item = {
                "id": organy[k][0],
                "name": organy[k][4],
                "abbreviation": organy[k][3]
            }
            try:
                item["color"] = pparties[organy[k][3]]['color']
            except Exception:
                item["color"] = "#888888"
            dw.writerow(item)


for row in zarazeni:
    if row[1] in groups and row[4] == '':
        data_obj[row[0]]['group'] = organy[row[1]][3]
        data_all_obj[row[0]]['group'] = organy[row[1]][3]

# regions, emails, names
regions = []
for k in organy:
    if organy[k][2] == region_number:
        regions.append(organy[k][0])

poslanec2 = {}
for k in poslanec:
    if poslanec[k][4] == term:
        poslanec2[poslanec[k][1]] = poslanec[k]

for k in data_obj:
    data_obj[k]['region'] = organy[poslanec2[k][2]][4]
    data_obj[k]['email'] = poslanec2[k][9]
    data_obj[k]['name'] = osoby[k][3] + " " + osoby[k][2]
    data_obj[k]['family_name'] = osoby[k][2]
    data_obj[k]['given_name'] = osoby[k][3]
    data_obj[k]['mp_id'] = poslanec2[k][0]

for k in data_all_obj:
    data_all_obj[k]['region'] = organy[poslanec2[k][2]][4]
    data_all_obj[k]['email'] = poslanec2[k][9]
    data_all_obj[k]['name'] = osoby[k][3] + " " + osoby[k][2]
    data_all_obj[k]['family_name'] = osoby[k][2]
    data_all_obj[k]['given_name'] = osoby[k][3]
    data_all_obj[k]['mp_id'] = poslanec2[k][0]

# other memberships
for m in memberships:
    ids = []
    for k in organy:
        if organy[k][1] == term and organy[k][2] == memberships[m]:
            ids.append(organy[k][0])

    for row in zarazeni:
        if row[1] in ids and row[4] == '':
            data_obj[row[0]][m].append(organy[row[1]][4])
            data_all_obj[row[0]][m].append(organy[row[1]][4])

for k in data_obj:
    data.append(data_obj[k])

for k in data_all_obj:
    data_all.append(data_all_obj[k])

data = sorted(sorted(data, key=lambda x: locale.strxfrm(x['given_name'])), key=lambda x: locale.strxfrm(x['family_name']))
data_all = sorted(sorted(data_all, key=lambda x: locale.strxfrm(x['given_name'])), key=lambda x: locale.strxfrm(x['family_name']))

# voters:
with open(path + "data/voters.csv", "w") as fout:
    header = ["id", "name", "given_name", "family_name"]
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    for row in data_all:
        dw.writerow({
            "id": row['id'],
            "name": row['name'],
            "given_name": row['given_name'],
            "family_name": row['family_name']
        })

# vote_events:
for anal in anals:
    with open(path + "data/vote_events" + anal['name'] + ".csv", "w") as fout:
        header = ["id", "start_date", "motion:text", "motion:result", "canceled", "sitting", "number", "point"]
        dw = csv.DictWriter(fout, header)
        dw.writeheader()
        for row in ve:
            d = row[5].split('.')
            nd = '-'.join([d[2], d[1], d[0]])
            if row[0] in zmatecne:
                canceled = "canceled"
            else:
                canceled = ""
            if row[14] == "A":
                result = "pass"
            else:
                result = "fail"
            item = {
                "id": row[0],
                "start_date": nd + 'T' + row[6] + ':00',
                "motion:text": row[15],
                "motion:result": result,
                "canceled": canceled,
                "sitting": row[2],
                "number": row[3],
                "point": row[4]
            }
            if int(item['id']) >= anal['min'] and int(item['id']) < anal['max']:
                dw.writerow(item)

# votes:
v2v = {
    'A': 'yes',
    'B': 'no',
    'C': 'abstain',
    'F': 'not voting',
    'K': 'abstain',
    '@': 'absent',
    'M': 'absent',
    'W': 'absent'
}
data_mp = {}
for k in data_all_obj:
    mp_id = data_all_obj[k]['mp_id']
    data_mp[mp_id] = data_all_obj[k]
for anal in anals:
    with open(path + "data/votes" + anal['name'] + ".csv", "w") as fout:
        header = ["vote_event_id", "voter_id", "option"]
        dw = csv.DictWriter(fout, header)
        dw.writeheader()
        len(votes)
        for row in votes:
            if int(row[1]) >= anal['min'] and int(row[1]) < anal['max']:
                dw.writerow({
                    "vote_event_id": row[1],
                    "voter_id": data_mp[row[0]]['id'],
                    "option": v2v[row[2]]
                })


# with open(path + "data.json", "w") as fout:
#     json.dump(data, fout, ensure_ascii=False)
#
# with open(path + "data_all.json", "w") as fout:
#     json.dump(data_all, fout, ensure_ascii=False)
