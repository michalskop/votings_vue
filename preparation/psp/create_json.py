"""Creates JSON as others JSONs."""

import csv
import json

path = '/home/michal/project/votings/dev/csv/psp/'

vote_event_id = "71888"

with open(path + "data/vote_events.csv") as fin:
    vote_events = {}
    dr = csv.DictReader(fin)
    for row in dr:
        vote_events[row['id']] = row

with open(path + "data/voters.csv") as fin:
    voters = {}
    dr = csv.DictReader(fin)
    for row in dr:
        voters[row['id']] = row

with open(path + "data/groups.csv") as fin:
    groups = {}
    dr = csv.DictReader(fin)
    for row in dr:
        groups[row['abbreviation']] = row

with open(path + "people.json") as fin:
    people = json.load(fin)
people_obj = {}
for row in people:
    people_obj[row['id']] = row

data = {
    "motion": {
        "name": vote_events[vote_event_id]['motion:text']
    },
    "end_date": vote_events[vote_event_id]['start_date'][0:10],
    "requirement": "Nadpoloviční většina přítomných"
}
with open(path + "data/votes.csv") as fin:
    votes = []
    dr = csv.DictReader(fin)
    for row in dr:
        if row['vote_event_id'] == vote_event_id:
            group_name = people_obj[row['voter_id']]['group']
            group = groups[group_name]
            votes.append({
                "voter": {
                    "name": people_obj[row['voter_id']]['name']
                },
                "group": {
                    "name": group_name,
                    "abbreviation": group_name,
                    "color": group['color']
                },
                "option": row['option']
            })
data['votes'] = votes

with open(path + "../psp2.json", "w") as fout:
    json.dump(data, fout, indent=4, ensure_ascii=False)
