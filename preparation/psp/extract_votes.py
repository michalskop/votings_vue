"""Download and extract votes."""

# NOT READY FOR MORE FILES !!!

import csv
import json

path = "/home/michal/dev/wpca/psp/cz-2017-2019/201907/"

with open(path + "people.json") as fin:
    people = json.load(fin)
# print(len(people))
people_obj = {}
for row in people:
    people_obj[row['mp_id']] = row

# Manually added:
people_obj['1586']['group'] = "ČSSD"
people_obj['1694']['group'] = "ANO"
people_obj['1608']['group'] = "KSČM"  # Koníček


ve_list = {}
with open(path + "hl2017s.unl", encoding="cp1250") as fin:
    csvr = csv.reader(fin, delimiter='|')
    for row in csvr:
        ve_list[row[0]] = row

v2v = {
    'A': 'yes',
    'B': 'no',
    'C': 'abstain',
    'F': 'not voting',
    'K': 'abstain',
    '@': 'absent',
    'M': 'absent',
    'W': 'absent'
}
votes = {}
with open(path + "hl2017h1.unl", encoding="cp1250") as fin:
    csvr = csv.reader(fin, delimiter='|')
    for row in csvr:
        try:
            votes[row[0]]
        except Exception:
            votes[row[0]] = {}
        votes[row[0]][row[1]] = row[2]

# print(len(ve_list))
# print(votes)


with open(path + "x_source.csv", "w") as fout:
    header = ['voter_id', 'vote_event_id', 'option']
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    for mp_id in votes:
        for ve_id in votes[mp_id]:
            try:
                item = {
                    'voter_id': people_obj[mp_id]['id'],
                    'vote_event_id': ve_id,
                    'option': v2v[votes[mp_id][ve_id]]
                }
            except Exception:
                item = {
                    'voter_id': '*' + mp_id,
                    'vote_event_id': ve_id,
                    'option': v2v[votes[mp_id][ve_id]]
                }
            dw.writerow(item)
