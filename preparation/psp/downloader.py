"""Download and extract data for analyses."""

import io
import json
import os
import requests
import zipfile

# path
try:
    path = os.path.dirname(os.path.realpath(__file__))
except Exception:
    path = os.getcwd()
path = "/home/michal/project/votings/dev/csv/psp/"


url = "http://www.psp.cz/eknih/cdrom/opendata/hl-2017ps.zip"
r = requests.get(url)
if r.ok:
    z = zipfile.ZipFile(io.BytesIO(r.content))
    z.extractall(path)

url = "http://www.psp.cz/eknih/cdrom/opendata/poslanci.zip"
r = requests.get(url)
if r.ok:
    z = zipfile.ZipFile(io.BytesIO(r.content))
    z.extractall(path)

url = "https://api.napistejim.cz/data_all.json"
r = requests.get(url)
if r.ok:
    with open(path + "people.json", "w") as fout:
        json.dump(r.json(), fout)
