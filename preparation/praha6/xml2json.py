"""Convert XML to json, one voting_event."""
import csv
import json
import xmltodict

path = "/home/michal/project/votings/dev/csv/"

with open(path + "praha6.xml") as fin:
    text = fin.read().replace('\n','').replace('\t','')
    o = xmltodict.parse(text)

excused = []
if isinstance(o['councilmeeting']['excused']['name'], list):
    for excuse in o['councilmeeting']['excused']['name']:
        excused.append(excuse)
else:
    try:
        excused.append(o['councilmeeting']['excused']['name'])
    except Exception:
        nothing = None


for ve in o['councilmeeting']['voting']:
    if ve['@order'] == '34':
        vote_event = ve

voted_options = {
    'votefor': [],
    'voteagainst': [],
    'voteabstain': []
}
for key in voted_options:
    if isinstance(vote_event[key]['name'], list):
        for vote_option in vote_event[key]['name']:
            voted_options[key].append(vote_option)
    else:
        try:
            voted_options[key].append(vote_event[key]['name'])
        except Exception:
            nothing = None

all = excused + voted_options['votefor'] + voted_options['voteagainst'] + voted_options['voteabstain']

# with open(path + 'praha6.csv', "w") as fout:
#     header = ['name']
#     dw = csv.DictWriter(fout, header)
#     dw.writeheader()
#     for row in all:
#         dw.writerow({'name': row})

with open(path + "praha6_list.csv") as fin:
    dr = csv.DictReader(fin)
    plist = []
    for row in dr:
        plist.append(row)

def _voted_option2vote(vo):
    if vo == 'votefor':
        return 'Pro'
    if vo == 'voteagainst':
        return 'Proti'
    if vo == 'voteabstain':
        return 'Zdržel/a se'
    else:
        return 'Nepřítomen/a'

data = []
for row in plist:
    item = row
    absent = True
    for key in voted_options:
        if row['name'] in voted_options[key]:
            item['option'] = _voted_option2vote(key)
            absent = False
    if absent:
        item['option'] = _voted_option2vote('')
    data.append(item)

with open(path + "praha6_ve.csv", "w") as fout:
    header = ['name', 'organization_name', 'organization_abbreviation', 'option']
    dw = csv.DictWriter(fout, header)
    dw.writeheader()
    for row in data:
        dw.writerow(row)
