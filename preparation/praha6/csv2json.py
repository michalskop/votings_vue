""" Convert CSV to JSON."""
import csv
import json

path = "/home/michal/project/votings/dev/csv/"

with open(path + "parties.csv") as fin:
    dr = csv.DictReader(fin)
    parties = []
    for row in dr:
        parties.append(row)


def _get_party(text, attribute):
    # print(text, attribute)
    for row in parties:
        # print('**', row[attribute], '**', text, '**', row[attribute] == text)
        if row[attribute] == text:
            return row
    return False

def _option2option_brno(option):
    if option == 'Pro':
        return 'yes'
    if option == 'Proti':
        return 'no'
    if option == 'Zdržel/a se':
        return 'abstain'
    if option == 'Nehlasoval/a':
        return 'not voting'
    if option == 'Nepřítomen/a':
        return 'absent'
    return 'unknown'

with open(path + "praha6_ve.csv") as fin:
    dr = csv.DictReader(fin)
    data = {
        "votes": [],
        "motion": {
            "name": "Návrh rozpočtu Městské části Praha 6 na rok 2015"
        },
        "end_date": "2015-10-21",
        "requirement": "Nadpoloviční většina všech členů"
    }
    for row in dr:
        item = {}
        item['voter'] = {"name": row['name'].strip()}
        item['group'] = {
            "name": row['organization_name'].strip(),
            "abbreviation": row['organization_abbreviation'].strip()
        }
        if row['organization_abbreviation'].strip() == 'MY':
            row['organization_abbreviation'] = 'ANO'
        color = _get_party(row['organization_abbreviation'].strip(), "abbreviation")
        if color:
            item['group']['color'] = color['color']
        else:
            item['group']['color'] = "#a7f63d"
        item['option'] = _option2option_brno(row['option'])
        data['votes'].append(item)

with open(path + "praha6.json", "w") as fout:
    json.dump(data, fout, indent=4, ensure_ascii=False)
